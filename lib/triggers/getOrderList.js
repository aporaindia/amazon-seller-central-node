var Q = require('q');
var elasticio = require('elasticio-node');
var messages = elasticio.messages;

exports.process = processTrigger;

function processTrigger(msg, cfg) {  
    
    var self = this;
    
    var accessKeyId = cfg.accessKeyId;
    var merchantId = cfg.merchantId;
    var marketplaceId = cfg.marketPlaceId;
    var secretKey = process.env.ASC_SECRET_ACCESS_KEY;

    console.log("accessKeyId : " + accessKeyId);
    console.log("merchantId : " + merchantId);
    console.log("marketplaceId : " + marketplaceId);
    console.log("secretKey : " + secretKey);

    function emitData() {
        var MWS = require('mws-sdk-promises'),
                client = new MWS.Client(accessKeyId, secretKey, merchantId, {'host': 'mws.amazonservices.in'}),
                MarketplaceId = marketplaceId;

        function getListOrders(client, args) {
            var req = MWS.Orders.requests.ListOrders();
            req.set(args);
            return client.invoke(req);
        }

        var date = new Date();
        getListOrders(client, {
            MarketplaceId: MarketplaceId,
            MaxResultsPerPage: 10,
            CreatedAfter: new Date(2015, 1, 1),
            CreatedBefore: new Date(2016, 1, 31)
        })
                .catch(function (error) {
                    console.error(error);
                })
                .then(function (RESULT) {
                    console.log("--------");
                    console.log(JSON.stringify(RESULT));
                    console.log("--------");

                    var data = messages.newMessageWithBody(RESULT);

                    console.log('Emitting data');

                    self.emit('data', data);
                });

        
    }

    function emitError(e) {
        console.log('Oops! Error occurred');

        self.emit('error', e);
    }

    function emitEnd() {
        console.log('Finished execution');

        self.emit('end');
    }

    Q().then(emitData).fail(emitError).done(emitEnd);
}